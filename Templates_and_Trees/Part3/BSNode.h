#pragma once

#include <string>
#include <iostream>


template<class type>


class BSNode
{
public:

	/*
	The function constructs the current BSNode class according to the given type
	Input: a type to put in the created node data
	*/
	BSNode(type data) :
		_data(data), _right(nullptr), _left(nullptr), _count(1)
	{
	}


	/*
	The function constructs the current BSNode class according to the given BSNode
	Input: a type to deep copy into the current node
	*/
	BSNode(const BSNode& other)
	{
		*this = other;
	}


	/*
	The function deconstructs the current BSNode class by deleting its right an left nodes
	*/
	~BSNode()
	{
		if (this != nullptr)
		{
			if (this->_right != nullptr)  //if the current right node isnt null to prevent any errors
			{
				delete this->_right;
			}
			if (this->_left != nullptr)  //if the current left node isnt null to prevent any errors
			{
				delete this->_left;
			}
		}
	}


	/*
	The function inserts the given type value in the correct place of the current binary tree
	Input: a type insert in the current binary tree
	*/
	void insert(type value)
	{
		if (value < this->_data)  //if the value is smaller than the current data, it needs to be placed in the left node
		{
			if (this->_left != nullptr)  //if the left node isnt empty it will keep seaching the right place of the value in the left node sub tree
			{
				this->_left->insert(value);
			}
			else  //if the left node is empty it will create a new node with the given value
			{
				this->_left = new BSNode(value);
			}
		}
		else if (value > this->_data)  //if the value is bigger than the current data, it needs to be placed in the right node
		{
			if (this->_right != nullptr)  //if the right node isnt empty it will keep seaching the right place of the value in the right node sub tree
			{
				this->_right->insert(value);
			}
			else  //if the right node is empty it will create a new node with the given value
			{
				this->_right = new BSNode(value);
			}
		}
		else if (this->_data == value)  //if the current node value is equal to the given value than count will be up by one
		{
			this->_count++;
		}
	}


	/*
	The function deep copy the given tree node to the current tree node
	Input: a tree node to copy to the current tree
	Output: the current tree but copied to the given one
	*/
	BSNode& operator=(const BSNode& other)
	{
		if (this == &other)
		{
			return *this;
		}
		else
		{
			this->_data = other.getData();
			this->_count = other.getCount();

			if (other.getRight() != nullptr)  //checks if the given tree node right is empty. if not empty it will copy it
			{
				if (this->_right != nullptr)  //checks if the current right node is empty, if not empty it will get deleted
				{
					delete this->_right;
				}

				this->_right = new BSNode(*other.getRight());
			}
			else
			{
				this->_right = nullptr;
			}

			if (other.getLeft() != nullptr)  //checks if the given tree node left is empty. if not empty it will copy it
			{
				if (this->_right != nullptr)  //checks if the current left node is empty, if not empty it will get deleted
				{
					delete this->_right;
				}

				this->_left = new BSNode(*other.getLeft());
			}
			else
			{
				this->_left = nullptr;
			}
		}
	}


	/*
	The function checks if the current tree node is a leaf (its right and left values are nullptr)
	Output: true if the current tree node is a leaf and false if not
	*/
	bool isLeaf() const
	{
		return ((this->_right == nullptr) && (this->_left == nullptr));
	}


	/*
	The functions returns the current tree node data
	*/
	type getData() const
	{
		return this->_data;
	}


	/*
	The function returns the current tree node left node
	*/
	BSNode* getLeft() const
	{
		return this->_left;
	}


	/*
	The function returns the current tree node right node
	*/
	BSNode* getRight() const
	{
		return this->_right;
	}


	/*
	The function returns teh current tree node value count element
	*/
	int getCount() const
	{
		return this->_count;
	}


	/*
	The function searches in the current tree node if the given value is in it
	Input: the value to search inside the current tree node
	Output: true if the value is inside the current tree node and false if not
	*/
	bool search(type val) const
	{
		if (this == nullptr)  //if the current tree node is empty it means we reached the end of the tree and the searched value is not here
		{
			return false;
		}
		else if (this->_data == val)  //if the current tree value is equal to the searched value it will return true because the value has been found in the tree node
		{
			return true;
		}

		return ((this->_right->search(val)) || (this->_left->search(val)));  //searching the vlaue in the left and right node of the current tree node
	}


	/*
	The function checks calclutes the current tree height
	Output: the current tree height
	*/
	int getHeight() const
	{
		int leftHeight = 0;
		int rightHeight = 0;

		if (this == nullptr || this->isLeaf())  //if the tree node has reached to the end, the return will be 0
		{
			return 0;
		}

		rightHeight = this->_right->getHeight() + 1;  //caluculates the height of the right node
		leftHeight = this->_left->getHeight() + 1;  //caluculates the height of the left node

		return std::max(leftHeight, rightHeight);  //gets the bigger value
	}


	/*
	The function searches the current tree node in the given root tree node and returns its depth if found
	Input: the root tree node to search in
	Output: -1 if the current tree node isnt found inside the given
	*/
	int getDepth(const BSNode& root) const
	{
		if (!(root.search(this->_data)))  //searches the current tree node inside the given tree node root
		{
			return -1;
		}

		return this->getCurrNodeDistFromInputNode(&root);  //if the current tree node is inside the given tree node, it will find its depth
	}


	/*
	The funciton prints the current tree node from the smalles value to the biggest
	*/
	void printNodes() const
	{
		if (this != nullptr)  //if the current tree node isnt empty it will print its data
		{
			this->_left->printNodes();  //prints the left node of the tree
			std::cout << this->getData() << std::endl;  //prints the current node's data
			this->_right->printNodes();  //prints the right node of the tree
		}

		return;
	}

private:

	//fields
	type _data;
	BSNode* _left;
	BSNode* _right;
	int _count;


	/*
	The function gets the depth of the given tree node according to the current tree node
	Input: tree node root to find the current tree node depth inside of it
	Output: the depth of the given tree node according to the current tree node
	*/
	int getCurrNodeDistFromInputNode(const BSNode* node) const
	{
		int leftDepth = 0;
		int rightDepth = 0;

		if (node == nullptr || node->isLeaf() || node == this)  //stops the recursion by returning 0 if the given tree node is empty of equal to the current node
		{
			return 0;
		}
		else
		{
			//finds the depth of the current node. according to its position (if smaller its on the left and if bigger its on the right)
			if (this->_data < node->getData())
			{
				return this->getCurrNodeDistFromInputNode(node->getLeft()) + 1;
			}
			else
			{
				return this->getCurrNodeDistFromInputNode(node->getRight()) + 1;
			}
		}
	}

};


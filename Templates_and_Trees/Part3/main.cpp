#include "BSNode.h"
#include <iostream>
#include <string>


#define ARR_SIZE 15


int main()
{
	int intArr[ARR_SIZE] = {13, 9, 8, 2, 4, 15, 10, 12, 7, 14, 11, 3, 1, 6, 5};  //numbers from 1 to 15
	BSNode<int>* intBS = new BSNode<int>(7);  //creates an int bst with the starting value of 7 (middle number between 1 and 15)

	std::string stringArr[ARR_SIZE] = { "o", "h", "a", "d", "k", "i", "n", "g", "c", "j", "b", "m", "e", "l", "f"};  //letters from a to o
	BSNode<std::string>* stringBS = new BSNode<std::string>("g");  //creates a string bst with the starting value of g (middle letter between a and 0)

	int i = 0;

	std::cout << "\nThe int array before sorting:" << std::endl;
	for (i = 0; i < ARR_SIZE; i++)  //prints and inserts the whole int array inside its binary tree
	{
		std::cout << intArr[i] << std::endl;
		intBS->insert(intArr[i]);
	}

	std::cout << "\nThe int array after sorting:" << std::endl;
	intBS->printNodes();

	std::cout << "\nThe string array before sorting:" << std::endl;
	for (i = 0; i < ARR_SIZE; i++)  //prints and inserts the whole string array inside its binary tree
	{
		std::cout << stringArr[i] << std::endl;
		stringBS->insert(stringArr[i]);
	}

	std::cout << "\nThe string array after sorting:" << std::endl;
	stringBS->printNodes();

	delete intBS;
	delete stringBS;
	return 0;
}


#include <iostream>
#include "functions.h"
#include "number.h"


int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<number>(number(1), number(2)) << std::endl;
	std::cout << compare<number>(number(2), number(1)) << std::endl;
	std::cout << compare<number>(number(1), number(1)) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	number doubleArr[arr_size] = { number(2), number(1), number(4), number(3), number(5) };
	bubbleSort<number>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<number>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}


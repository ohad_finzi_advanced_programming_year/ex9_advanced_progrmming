#include <iostream>


template<class type>
int compare(type first, type second)
{
	int ans = 0;

	if (first > second)
	{
		ans = -1;
	}
	else if (first < second)
	{
		ans = 1;
	}
	else
	{
		ans = 0;
	}

	return ans;
}


template <class type>
void swap(type& first, type& second)
{
	type temp = first;
	first = second;
	second = temp;
}


template<class type>
void bubbleSort(type arr[], int n)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap<type>(arr[j], arr[j + 1]);
			}
		}
	}
}


template<class type>
void printArray(type arr[], int n)
{
	int i = 0;

	for (i = 0; i < n; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}


#include <iostream>


class number
{
public:
	int _val;

	number(int val) :
		_val(val)
	{}

	friend std::ostream& operator<<(std::ostream& os, const number& n)
	{
		os << n._val;
		return os;
	}

	bool operator==(const number& other) const
	{
		return this->_val == other._val;
	}

	bool operator<(const number& other) const
	{
		return this->_val < other._val;
	}

	bool operator>(const number& other) const
	{
		return this->_val > other._val;
	}

	number& operator=(const number& other)
	{
		this->_val = other._val;
		return *this;
	}
};


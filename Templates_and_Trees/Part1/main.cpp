#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

//MEMORY LEAK LIBRARIES
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

using std::cout;
using std::endl;


void checkTreeFunctions();


int main()
{
	checkTreeFunctions();

	//MEMORY LEAK CHECK
	printf("Leaks: %d", _CrtDumpMemoryLeaks());
	return 0;
}


void checkTreeFunctions()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	
	std::cout << bs->search("8") << std::endl;  //should be 1
	std::cout << bs->search("1") << std::endl;  //should be 0

	std::string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");

	remove(textTree.c_str());
	delete bs;
}

